# newmac_hotspot.sh

This script generates a new MAC for a given interface, then opens a google page. This behaviour is intended to suit hotspot behaviour.

It optionally accepts an interface as the only argument.

## Usage 
Usage: ./newmac_hotspot.sh [INTERFACE]

If no INTERFACE is given, `wlan0` is assumed as default.

This script should be executed as root.
