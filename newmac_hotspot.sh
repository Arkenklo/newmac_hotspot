#!/bin/bash
# This script generates a new MAC for a given interface, by default wlan0,
# and opens a website for hotspot login to kick in

### Pre-checks

# Check if executed as root
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Check if we were given an interface as an argument
if [ -n "$1" ]; then
	interface=$1
else
	interface="wlan0"
fi


### Generate MAC address

# Generate a random MAC based on current vendor bytes
current_mac="$(cat /sys/class/net/$interface/address)"

# Now get the vendor bytes
current_vendor="$(echo $current_mac | cut -b1-8)"

# Generate a new MAC using our current vendor bytes plus three random pairs
# TODO: Make this cleaner and more readable
new_mac="$(printf "$current_vendor:%02X:%02X:%02X\n" $[RANDOM%256] $[RANDOM%256] $[RANDOM%256])"


### Assign new MAC to interface

# Bring the interface down
echo "Disabling network-manager and interface"
service network-manager stop
ifconfig $interface down

# Bning it back up
echo "Enabling network-manager and interface"
ifconfig $interface hw ether $new_mac up
service network-manager start


### Prompt user to accept hotspot's terms and conditions

echo "Opening Firefox. Please accept terms and conditions."
timeout 180 firefox google.com
echo "Done"